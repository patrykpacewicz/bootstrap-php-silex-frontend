include apt

apt::ppa { 'ppa:nginx/stable': }

package { 'nginx':
  ensure  => installed,
  require => Apt::Ppa['ppa:nginx/stable'],
  notify  => Service['nginx']
}

file { '/etc/nginx/sites-enabled/default' :
  ensure  => absent,
  require => Package['nginx']
}

file { "/etc/nginx/conf.d/${cfg_codeName}.conf":
  ensure  => file,
  content => template("${cfg_templatesDir}/nginx.cfg"),
  require => Package['nginx'],
  notify  => Service['nginx'],
}

service { 'nginx':
  ensure  => running,
  require => Package['nginx'],
}
