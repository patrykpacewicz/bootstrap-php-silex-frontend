exec { 'app install':
  command     => "composer.phar install",
  path        => ['/usr/bin'],
  cwd         => $cfg_rootDir,
  user        => $cfg_script_user,
  logoutput   => true,
  environment => ["HOME=/home/${cfg_script_user}"],
  require     => Package['php5-cli'],
  timeout     => 600,
  tries       => 3
}

exec { 'app build':
  command     => "phing build",
  path        => ['/usr/bin', 'bin'],
  cwd         => $cfg_rootDir,
  user        => $cfg_script_user,
  logoutput   => true,
  environment => ["HOME=/home/${cfg_script_user}"],
  require     => [
    Exec['app install'],
    Package['php5-cli'],
    Package['grunt-cli']
  ],
  timeout     => 600,
  tries       => 3
}
