$cfg_codeName        = $::appname
$cfg_debug           = str2bool($::appdebug)
$cfg_debug_num       = bool2num($cfg_debug)
$cfg_rootDir         = $::appdir
$cfg_script_user     = $::appscriptuser
$cfg_templatesDir    = "${::appdir}/deployment/templates"
$cfg_logDir          = "/var/log/${cfg_codeName}"

$cfg_additionalPackages = ['git', 'curl', 'vim']

$cfg_user  = 'www-data'
$cfg_group = 'www-data'

$cfg_php_listen_sock = "/var/run/php5-fpm-${::appname}.sock"
$cfg_php_timezone    = "Europe/Warsaw"
