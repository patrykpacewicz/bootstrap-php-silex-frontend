<?php

namespace PatrykPacewicz\Hello\Controller;

use \Twig_Environment;

class DefaultController
{
    /** @var Twig_Environment */
    private $twig;

    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function defaultAction()
    {
        $parameters = ['message' => 'Hello World!'];
        return $this->twig->render('index.html.twig', $parameters);
    }
}
