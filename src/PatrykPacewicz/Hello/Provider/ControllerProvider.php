<?php

namespace PatrykPacewicz\Hello\Provider;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class ControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $this->configureRouting($controllers);
        return $controllers;
    }

    private function configureRouting(ControllerCollection $controllers)
    {
        $controllers->get('/', "pp.controller.default:defaultAction")->bind('homepage');
    }
}
