<?php

namespace PatrykPacewicz\Hello\Provider;

use PatrykPacewicz\Hello\Controller\DefaultController;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $this->errorProvider($app);

        $app['pp.controller.default'] = $app->share(function ($app) {
            return new DefaultController($app['twig']);
        });
    }

    public function boot(Application $app)
    {
        $app['pp.config'] = Yaml::parse('');
    }

    public function errorProvider(Application $app)
    {
        $app->error(function ($code) use ($app) {
            if ($app['debug']) {
                return null;
            }

            if ($code === 404) {
                return $this->errorAction($app['twig'], 404, 'Page not found');
            }

            return $this->errorAction($app['twig'], 500, 'Internal server error');
        });
    }

    private function errorAction(Twig_Environment $twig, $code, $message)
    {
        $parameters = ['message' => $message, 'code' => $code];
        $errorContent = $twig->render('error.html.twig', $parameters);

        return new Response($errorContent, $code);
    }
}
