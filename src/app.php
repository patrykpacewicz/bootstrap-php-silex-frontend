<?php

use Monolog\Logger;
use PatrykPacewicz\Hello\Provider\ControllerProvider;
use PatrykPacewicz\Hello\Provider\ServiceProvider;
use Silex\Application;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new UrlGeneratorServiceProvider());
$app->register(new TwigServiceProvider(), array('twig.path' => __DIR__.'/PatrykPacewicz/Hello/Resources/views'));
$app->register(
    new ServiceProvider(),
    array('config.file' => __DIR__.'/PatrykPacewicz/Hello/Resources/config/config.yml')
);
$app->register(
    new MonologServiceProvider(),
    array(
        'monolog.logfile' => sprintf('%s/%s.php.log', $_SERVER['logdir'], $_SERVER['appname']),
        'monolog.level'   => $_SERVER['debug']? Logger::DEBUG : Logger::WARNING,
        'monolog.name'    => $_SERVER['appname']
    )
);

$app->mount('/', new ControllerProvider());

$app['debug'] = $_SERVER['debug'];

return $app;
