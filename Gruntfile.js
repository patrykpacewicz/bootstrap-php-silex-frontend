module.exports = function(grunt) {
    grunt.initConfig({
        clean:  ["web/fonts", "web/scripts", "web/styles"],
        symlink: {
            development: { src: 'src/PatrykPacewicz/Hello/Resources/scripts', dest: 'web/scripts'}
        },
        requirejs: {
            options: {
                appDir: 'src/PatrykPacewicz/Hello/Resources/scripts',
                dir: 'web/scripts',
                baseUrl: '.',
                paths: {
                    'require':    'vendor/require-2.1.8',
                    'jquery':     'vendor/jquery-1.10.2',
                    'bootstrap':  'vendor/bootstrap-3.0.0'
                },

                shim: {
                    "bootstrap": { deps: ["jquery"] }
                },

                modules: [ { name: 'app' } ]
            },
            production: {},
            development: { options: { optimize: "none" } }
        },
        less: {
            production: {
                options: { compress: true },
                files:   { 'web/styles/style.css': 'src/**/Resources/less/index.less' }
            },
            development: {
                options: { compress: false },
                files:   { 'web/styles/style.css': 'src/**/Resources/less/index.less' }
            }
        },
        watch: {
            less: {
                files: ['src/**/Resources/less/**/*.less'],
                tasks: ['less:development']
            }
        },
        copy: {
            main: {
                files: [
                    {expand: true, filter: 'isFile', flatten: true, src: ['src/**/Resources/fonts/*'], dest: 'web/fonts/' },
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-symlink');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', ['build:production']);
    grunt.registerTask('build:production',  ['clean', 'copy', 'less:production',  'requirejs:production']);
    grunt.registerTask('build:development', ['clean', 'copy', 'less:development', 'symlink:development']);
};