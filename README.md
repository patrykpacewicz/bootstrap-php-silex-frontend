[![Build Status](https://drone.io/bitbucket.org/patrykpacewicz/bootstrap-php-silex-frontend/status.png)](https://drone.io/bitbucket.org/patrykpacewicz/bootstrap-php-silex-frontend/latest)

Bootstrap - PHP - Frontend - Silex
==================================
Code is prepared so you can easily start working on a new applications.
It has production and development configuration.

Launch for development
----------------------
Application is connected to the vagrant project,
which will prepare the virtual machine and configure it for proper working.
```
vagrant up
```
Enjoy [http://localhost:8881/](http://localhost:8881/)  :-)

Launch on production environment
--------------------------------
Application is also prepared for easy production deployment
```
git clone https://bitbucket.org/patrykpacewicz/bootstrap-php-silex-frontend.git /app/frontend
cd /app/frontend
deployment/bootstrap.sh
```

What's inside ?
---------------
 * Silex + Twig
 * Phing build tool ( build, clean, info, test, watch )
 * Twitter Bootstrap
 * RequireJS + jQuery
 * GruntJS for building and watching CSS, JS
 * Puppet for server configuration
 * Example: 'Hello World!'
